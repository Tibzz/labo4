/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: Memento.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package memento;


public class Memento<T>{
	private T object;

	/**
	 * Constructeur
	 * @param object
	 */
	public   Memento(T object){this.object = object;}
	
	/**
	 * @return objet
	 */
	public T getState(){return object; }
}
