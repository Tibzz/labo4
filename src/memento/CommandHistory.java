/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: CommandHistory.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package memento;

import controlleur.*;

import java.util.Stack;




public class CommandHistory {

	private static CommandHistory cmdHistory;
	private Stack<Memento<Command>> history = new Stack<Memento<Command>>();
	
    private CommandHistory() {

    }

    public static CommandHistory getInstance() {
        if(cmdHistory == null)
        	cmdHistory = new CommandHistory();
        return cmdHistory;
    }
    
    public Memento<Command> getDerniereCommande(){
		if(history.isEmpty())
				return null;
    	return history.pop();
    }
    
    public Memento<Command> peekDerniereCommande(){
		if(history.isEmpty())
				return null;
    	return history.peek();
    }
    
    public void setDerniereCommande(Memento<Command> m){
    	history.add(m);
    }
}
