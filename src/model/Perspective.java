/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: Perspective.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package model;

import java.awt.Point;
import java.util.Observable;

public class Perspective extends Observable{

	private Point origine;
	private float zoom;
	
	
	public Perspective(){
		origine = new Point(0,0);
		zoom = 1;
		ModelImage.stock(this);
	}
	
	public void notifi(){
		super.setChanged();
		super.notifyObservers();
	}
	
	public void setOrigine(Point origine) {
		this.origine = origine;
	}

	public void setZoom(float zoom) {
		this.zoom = zoom;
	}

	
	public Point getOrigine(){
		return origine;	
	}
	
	public float getZoom(){
		return zoom;
	}
	
	public String toString(){
		return "Perspective";
	}
}

