/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: ModelImage.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package model;

import javax.imageio.ImageIO;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Observable;





public class ModelImage extends Observable{

	private static ModelImage instance;
	
	private static Perspective pptive1;
	private static Perspective pptive2;
	
/*	public void setImage(File f){
		try {
			image = ImageIO.read(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
    public static ModelImage getInstance() {
        if(instance == null)
        	instance = new ModelImage();
        return instance;
    }
	
	
	
	public static void stock(Perspective p){
		if(pptive1==null){
			pptive1=p;}
		else
			pptive2=p;
		
	}
	
	public void notifi(){
		super.setChanged();
		super.notifyObservers();
	}
	
	public static void setPerspective(Point p, float zoom){
		pptive1.setOrigine(p);
		pptive2.setOrigine(p);
		pptive1.setZoom(zoom);
		pptive2.setZoom(zoom);
	}
	
	public static void setPerspective(Point p1,Point p2, float zoom1, float zoom2){
		pptive1.setOrigine(p1);
		pptive2.setOrigine(p2);
		pptive1.setZoom(zoom1);
		pptive2.setZoom(zoom2);
	}
}
