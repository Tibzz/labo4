package model;

import java.awt.Dimension;

public class ModelThumbnail {
	
	public static Dimension redimensionnerThumb(Dimension dimension, double max){
		
		Dimension dim = new Dimension();
		if(dimension.getHeight() > max && dimension.getWidth() <= max ){
			dim.setSize(((dimension.getWidth()*max)/dimension.getHeight()), max);	
		}
		else if(dimension.getWidth() > max && dimension.getHeight() <= max ){
			dim.setSize(max, ((dimension.getHeight()*max)/dimension.getWidth()));	
		}
		else if(dimension.getHeight() > max && dimension.getWidth() > max ){
			if(dimension.getHeight() > dimension.getWidth()){
				dim.setSize((dimension.getWidth()*max)/dimension.getHeight(), max);
			}
			else if(dimension.getHeight() < dimension.getWidth()){
				dim.setSize(max, ((dimension.getHeight()*max)/dimension.getWidth()));
			}
		}
		else {
			dim = dimension;
		}
		return dim;
	}
	
	
}
