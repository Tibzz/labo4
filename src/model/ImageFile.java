/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: ImageFile.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package model;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Observable;


public class ImageFile extends Observable{
	
    private static ImageFile instance;

    private static Image image;
    private File file;
    
    public int height;
    public int width;

	

    public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	/**
     * Constructeur par défaut de la classe
     */
    private ImageFile(){};

    /**
     * Fonction permettant de récupèrer l'instance de la classe ImageFile
     * @return l'instance d'ImageFile
     */
    public static ImageFile getInstance() {
        if(instance == null)
        	instance = new ImageFile();
        return instance;
    }
    
	public void notifi(){
		super.setChanged();
		super.notifyObservers();
	}
    
	public void setImage(File f){
		try {
			file = f;
			image = ImageIO.read(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Image getImage(){
		return image;
	}
	
	public File getFile(){
		return file;
	}
	
	public String toString(){
		return "ImageFile";
	}
	
	
	public Dimension getDimensionImage(){
		return new Dimension(image.getWidth(null),image.getHeight(null));
		
	}
	
	public Dimension redimensionner(Dimension dimension, double max){
		
		Dimension dim = new Dimension();
		if(dimension.getHeight() > max && dimension.getWidth() <= max ){
			dim.setSize(((dimension.getWidth()*max)/dimension.getHeight()), max);	
		}
		else if(dimension.getWidth() > max && dimension.getHeight() <= max ){
			dim.setSize(max, ((dimension.getHeight()*max)/dimension.getWidth()));	
		}
		else if(dimension.getHeight() > max && dimension.getWidth() > max ){
			if(dimension.getHeight() > dimension.getWidth()){
				dim.setSize((dimension.getWidth()*max)/dimension.getHeight(), max);
			}
			else if(dimension.getHeight() < dimension.getWidth()){
				dim.setSize(max, ((dimension.getHeight()*max)/dimension.getWidth()));
			}
		}
		else {
			dim = dimension;
		}
		this.width = dim.width;
		this.height = dim.height;
		return dim;
	}
	
    public String imageChooser(){
    	
    	String path = null;
    	
    	// On définit les extensions que l'on accepte
    	FileNameExtensionFilter imagesFilter = new FileNameExtensionFilter("Images", "bmp", "gif", "jpg", "jpeg", "png");
    	 
    	// Ouverture du Gestionnaire de fichier
    	JFileChooser choix = new JFileChooser(new File("Data"));
    	choix.setDialogTitle("Choisir une image");
    	choix.addChoosableFileFilter(imagesFilter);
    	int retour = choix.showOpenDialog(null);
    	
    	//Si un fichier est choisi
    	if (retour == JFileChooser.APPROVE_OPTION) {
    		//On recupère le nom complet
    	    path = choix.getSelectedFile().getAbsolutePath();
    	}
    	else{
    		System.out.println("Le fichier n'a pas pu être ouvert.");
    	}
    	
    	return path;
	}
    
}
