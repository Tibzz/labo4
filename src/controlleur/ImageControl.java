/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: ImageControl.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package controlleur;


import memento.CommandHistory;
import model.ImageFile;
import model.ModelImage;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class ImageControl implements ActionListener{


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand()=="Sauvegarder"){
		
			
			
		}
		
		else{
		
		ImageFile.getInstance().setImage(new File(ImageFile.getInstance().imageChooser()));
		ModelImage.setPerspective(new Point(0,0), 1);
		ImageFile.getInstance().notifi();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException f) {
			f.printStackTrace();
		}
		ModelImage.getInstance().notifi();
		}
	}
	
	public static void undo(){
		try {
			CommandHistory.getInstance().peekDerniereCommande().getState().undo();
			CommandHistory.getInstance().getDerniereCommande().getState().getModel().notifi();
		}catch(NullPointerException e) {
	            System.out.println("Aucune annulation disponible");
	        }
	}
	

}
