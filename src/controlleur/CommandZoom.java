/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: CommandZoom.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package controlleur;

import java.awt.Image;
import java.awt.Point;

import View.ImagePanel;
import memento.CommandHistory;
import memento.Memento;
import model.ImageFile;
import model.Perspective;

public class CommandZoom implements Command{


	private Perspective pptive;
	Point coord;
	private Point ancCoord;
	private float ancZoom;
	Image image;
	
	public CommandZoom(Perspective p){
		this.pptive = p;
		ancCoord = new Point();
	}
	
	@Override
	public void execute(String s) {
		
		ancCoord.setLocation(pptive.getOrigine());
		ancZoom = pptive.getZoom();
		image = ImageFile.getInstance().getImage();
		
		float xImage = pptive.getOrigine().x+((image.getWidth(null)*pptive.getZoom())/ImageFile.getInstance().getWidth())*coord.x;
		float yImage = pptive.getOrigine().y+((image.getHeight(null)*pptive.getZoom())/ImageFile.getInstance().getHeight())*coord.y;
		
		if(s.equals("ZoomIn")){
			
			   if(pptive.getZoom()>0.1){
				   pptive.setZoom(pptive.getZoom()-0.1f);
				   
				   float width = (image.getWidth(null)*pptive.getZoom());
				   float height = (image.getHeight(null)*pptive.getZoom());
				   
				   
				   if(xImage-width*0.5 < 0)
					   pptive.getOrigine().x = 0;
				   else if(xImage+width*0.5 > ancCoord.x+ancZoom*image.getWidth(null))
					
					   pptive.getOrigine().x = (int)(ancCoord.x+ancZoom*image.getWidth(null) - width);
				   
				   else{
					   pptive.getOrigine().x = (int)(xImage- (width*0.5));
				   }
				   
				   if(yImage-height*0.5 < 0)
					   pptive.getOrigine().y = 0;
				   
				   else if(yImage+height*0.5> ancCoord.y+ancZoom*image.getHeight(null))
					   pptive.getOrigine().y = (int)(ancCoord.y+ancZoom*image.getHeight(null) - height);
				   else{
					   pptive.getOrigine().y = (int)(yImage-(height*0.5));
				   }
			   }
		}
		
		else{
			   if(pptive.getZoom()<1){
				   pptive.setZoom(pptive.getZoom()+0.1f);
				   
				   float width = (image.getWidth(null)*pptive.getZoom());
				   float height = (image.getHeight(null)*pptive.getZoom());
				   
				   
				   if(xImage-width*0.5<0)
					   pptive.getOrigine().x = 0;
				   
				   else if(xImage+width*0.5>image.getWidth(null))
					   pptive.getOrigine().x = (int)(image.getWidth(null)-width);
				   
				   else
					   pptive.getOrigine().x = (int)(xImage-(width*0.5));
				   
				   
				   if(yImage-height*0.5<0)
					   pptive.getOrigine().y = 0;
				   
				   else if(yImage+height*0.5>image.getHeight(null))
					   pptive.getOrigine().y = (int)(image.getHeight(null)-height);
				   
				   else
					   pptive.getOrigine().y = (int)(yImage-(height*0.5));
				   
				   if(pptive.getZoom()==1){ 
					   pptive.getOrigine().setLocation(0, 0);
				   }
			   }
		}
			   
		
	}

	@Override
	public void undo() {
		pptive.getOrigine().x = this.ancCoord.x;
		pptive.getOrigine().y = this.ancCoord.y;
		pptive.setZoom(ancZoom);
		
	}

	@Override
	public void createMemento() {
		CommandHistory.getInstance().setDerniereCommande(new Memento<Command>(clone()));
		
	}

	public CommandZoom clone(){
		CommandZoom command = new CommandZoom(pptive);
		command.ancCoord.setLocation(this.ancCoord);
		command.ancZoom = this.ancZoom;
		return command;
	}
	
	@Override
	public void setCoord(Point p) {
		this.coord=p;
		
	}
	
	public Perspective getModel() {
		return pptive;
		
	}

}
