/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: CommandMove.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package controlleur;

import java.awt.Image;
import java.awt.Point;

import View.ImagePanel;
import model.ImageFile;
import model.Perspective;
import memento.*;

public class CommandMove implements Command{

	
	Perspective pptive;
	Point coord;
	private Point ancCoord;
	Image image;
	
	
	public CommandMove(Perspective p){
		this.pptive = p;
		ancCoord = new Point();
	}
	
	@Override
	public void execute(String s) {
		image = ImageFile.getInstance().getImage();
		
		
		pptive.getOrigine().x = (int)(pptive.getOrigine().x-((image.getWidth(null)*pptive.getZoom())/ImageFile.getInstance().getWidth())*coord.x);
		pptive.getOrigine().y = (int)(pptive.getOrigine().y-((image.getHeight(null)*pptive.getZoom())/ImageFile.getInstance().getHeight())*coord.y);
	}

	@Override
	public void undo() {
		pptive.getOrigine().x = ancCoord.x;
		pptive.getOrigine().y = ancCoord.y;
	}

	@Override
	public void createMemento() {
		CommandHistory.getInstance().setDerniereCommande(new Memento<Command>(clone()));
		
	}
	
	public CommandMove clone(){
		CommandMove command = new CommandMove(pptive);
		command.ancCoord.setLocation(pptive.getOrigine().x, pptive.getOrigine().y);
		return command;
	}

	@Override
	public void setCoord(Point p) {
		this.coord = p;
		
	}
	
	public Perspective getModel() {
		return pptive;
		
	}

}
