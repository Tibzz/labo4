/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: Command.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/
package controlleur;

import java.awt.Point;

import model.Perspective;

public interface Command {

		
		void execute(String s);
		
		public void undo();
		
		public void createMemento();
		
		void setCoord(Point p);
		
		Perspective getModel();
		
		
		
	}
