/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: PerspectiveControl.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package controlleur;


import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import memento.CommandHistory;
import model.Perspective;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class PerspectiveControl implements MouseWheelListener, MouseMotionListener{

	private Perspective model;
	private Command zoom;
	private Command move;
	int i=0;
	int j=0;


	public static void undo(){
		try {
			CommandHistory.getInstance().peekDerniereCommande().getState().undo();
			CommandHistory.getInstance().getDerniereCommande().getState().getModel().notifi();
		}catch(NullPointerException e) {
	            System.out.println("Aucune annulation disponible");
	        }
	}
	
	
	public PerspectiveControl(Perspective p){
		model = p;
		zoom = new CommandZoom(p);
		move = new CommandMove(p);
	}
	
	public void reset(){
		i=0;
		j=0;
		move.createMemento();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {

		if(i==0 && j==0){
			i= e.getX();
			j= e.getY();
		}
		i= e.getX()-i;
		j= e.getY()-j;
		
		move.setCoord(new Point(i,j));
		move.execute("s");
		model.notifi();
		i= e.getX();
		j= e.getY();
	}



	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		;
		int notches = e.getWheelRotation();
		zoom.setCoord(new Point(e.getX(),e.getY()));  
		
		if (notches < 0) {
	    	   zoom.execute("ZoomIn");
	    	   zoom.createMemento();
	       }
	       else{
	    	   	zoom.execute("ZoomOut"); 
	       		zoom.createMemento();
	       }
		model.notifi();
	}
	


}
