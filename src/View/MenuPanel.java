/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: MenuPanel.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package View;

import model.ImageFile;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.KeyStroke;

import controlleur.ImageControl;
import controlleur.PerspectiveControl;


public class MenuPanel extends JMenuBar {

    private static final String MENU_FILE_TITLE = "Fichier",
                                MENU_FILE_QUIT = "Quitter",
                                MENU_FILE_OPEN = "Ouvrir",
                                MENU_FILE_OPEN_IMAGE = "Ouvrir ImageFile",
                                MENU_FILE_SAVE = "Sauvegarder",
                                MENU_EDIT_TITLE = "Editer",
                                MENU_EDIT_UNDO = "Annuler",
                                MENU_EDIT_REDO = "Rétablir",
                                MENU_EDIT_ZOOM = "Zoom",
                                MENU_EDIT_MOVE = "Déplacer",
                                MENU_HELP_TITLE = "Aide",
                                MENU_HELP_ABOUT = "A propos";

    private static final char MOVE_UP_SC = KeyEvent.VK_UP;
    private static final char MOVE_DOWN_SC = KeyEvent.VK_DOWN;
    private static final char MOVE_LEFT_SC = KeyEvent.VK_LEFT;
    private static final char MOVE_RIGHT_SC = KeyEvent.VK_RIGHT;

    private static final char ZOOM_PLUS_SC = KeyEvent.VK_PLUS;
    private static final char ZOOM_MINUS_SC = KeyEvent.VK_MINUS;

    private static final int CTRL_MASK = KeyEvent.CTRL_DOWN_MASK;

    private static final char MENU_FILE_QUIT_SC = KeyEvent.VK_W;
    private static final int MENU_FILE_OPEN_SC = KeyEvent.VK_O;
    private static final char MENU_FILE_SAVE_SC = KeyEvent.VK_S;
    private static final char MENU_EDIT_UNDO_SC = KeyEvent.VK_Z;
    private static final char MENU_EDIT_REDO_SC = KeyEvent.VK_Y;
    private static final char MENU_EDIT_MOVE_SC = KeyEvent.VK_M;
    
    private ImageControl ctrImg;

    //private ImageFile image;

    /**
     * Constructeur du menu
     */
    public MenuPanel(ImageControl i){
    	
    	ctrImg = i;
        addMenuFile();
        addMenuEdit();
        addMenuHelp();
    }

    private void addMenuFile(){

        JMenu menuFile = creer(MENU_FILE_TITLE, new String[]{MENU_FILE_OPEN, MENU_FILE_OPEN_IMAGE, MENU_FILE_SAVE,
                MENU_FILE_QUIT});

        menuFile.getItem(1).addActionListener(ctrImg);
        menuFile.getItem(1).setAccelerator(
                KeyStroke.getKeyStroke(
                        MENU_FILE_OPEN_SC,
                        CTRL_MASK));

        menuFile.getItem(2).addActionListener(ctrImg);
        menuFile.getItem(2).setAccelerator(
                KeyStroke.getKeyStroke(
                        MENU_FILE_SAVE_SC,
                        CTRL_MASK));

        menuFile.getItem(3).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });
        menuFile.getItem(3).setAccelerator(
                KeyStroke.getKeyStroke(
                        MENU_FILE_QUIT_SC,
                        CTRL_MASK));

        add(menuFile);

    }

    private void addMenuEdit(){

        JMenu menuEdit = creer(MENU_EDIT_TITLE, new String[] {MENU_EDIT_UNDO, MENU_EDIT_REDO, MENU_EDIT_ZOOM, MENU_EDIT_MOVE});

        menuEdit.getItem(0).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PerspectiveControl.undo();
            }
        });
        menuEdit.getItem(0).setAccelerator(
                KeyStroke.getKeyStroke(
                        MENU_EDIT_UNDO_SC,
                        CTRL_MASK));

        menuEdit.getItem(1).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //TODO
            }
        });
        menuEdit.getItem(1).setAccelerator(
                KeyStroke.getKeyStroke(
                        MENU_EDIT_REDO_SC,
                        CTRL_MASK));

        menuEdit.getItem(3).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //TODO
            }
        });
        menuEdit.getItem(3).setAccelerator(
                KeyStroke.getKeyStroke(
                        MENU_EDIT_MOVE_SC,
                        CTRL_MASK));

        add(menuEdit);
    }

    private void addMenuHelp(){

        JMenu menuHelp = creer(MENU_HELP_TITLE, new String[] {MENU_HELP_ABOUT});
        add(menuHelp);
    }

    private JMenu creer(String title, String[] items){

        JMenu menu = new JMenu(title);

        for(int i = 0; i < items.length; i++){
            menu.add(new JMenuItem(items[i]));
        }

        return menu;
    }
}
