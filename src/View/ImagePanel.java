/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: ImagePanel.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package View;

import javax.swing.*;

import controlleur.PerspectiveControl;
import model.ImageFile;
import model.Perspective;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class ImagePanel extends JComponent implements Observer{
	
	public static final double DIMENSION_MAX = 700;


    public Dimension dimension;

    private Image image;
    private PerspectiveControl ctr;
    private Perspective pptive;
    
    public ImagePanel() {
    	//On instancie la taille de la vue imagePanel de manniere a garder une image
    	//proportionelle a celle d'origine si elle est trop grande
        setPreferredSize(ImageFile.getInstance().redimensionner(ImageFile.getInstance().getDimensionImage(), DIMENSION_MAX));
    	        
        image = ImageFile.getInstance().getImage();
        ImageFile.getInstance().addObserver(this);
        pptive = new Perspective();
        pptive.addObserver(this);
        ctr = new PerspectiveControl(pptive);
        this.addMouseWheelListener(ctr);
        this.addMouseListener(new MouseAdapter(){
        	public void mousePressed(MouseEvent e) {
			      ctr.reset();


			   }
		});
        this.addMouseMotionListener(ctr);
       // this.getParent().reset();

    }
    

    protected void paintComponent(Graphics g){
    	g.drawImage(image,0,0,(int)ImageFile.getInstance().getWidth(),
    				(int)ImageFile.getInstance().getHeight(),
    				pptive.getOrigine().x,pptive.getOrigine().y,
    				pptive.getOrigine().x+(int)(image.getWidth(null)*pptive.getZoom()),
    				pptive.getOrigine().y+(int)(image.getHeight(null)*pptive.getZoom()),
    				null);
    }
    
	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.print(this.getParent().getParent().toString());
		if (arg0.toString()=="ImageFile"){
			image = ImageFile.getInstance().getImage();
			setPreferredSize(ImageFile.getInstance().redimensionner(ImageFile.getInstance().getDimensionImage(), DIMENSION_MAX));
		}
		else		
		repaint();
		
	}	
	
}

