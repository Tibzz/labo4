/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: MainFrame.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package View;

import javax.swing.*;

import model.ImageFile;
import model.ModelImage;
import model.ModelImage;
import controlleur.ImageControl;

import javax.swing.*;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class MainFrame extends JFrame implements Observer{

    //public static final Dimension dimension = new Dimension(500,800);

	private Thumbnail thumbnail;
	private JSplitPane split;
	private Views views;
    /**
     * Constructeur
     */
    public MainFrame(ImageControl c){


        this.setTitle("Image Viewer");

        MenuPanel menu = new MenuPanel(c);
        this.add(menu, BorderLayout.NORTH);
        ModelImage.getInstance().addObserver(this);

        ImagePanel view1 = new ImagePanel();
        ImagePanel view2 = new ImagePanel();
        thumbnail = new Thumbnail();
        
        split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, view1, view2);
        views = new Views(split, thumbnail);

        this.add(views, BorderLayout.CENTER);

        this.pack();
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //Arrêt du programme à la fermeture de la fenêtre
    }


    public void reset(){
   	 
    	this.remove(views);
   	 	split.resetToPreferredSizes();
        views = new Views(split, thumbnail);
        this.add(views, BorderLayout.CENTER);
        this.pack();
   }
    
    private class Views extends JLayeredPane{

        /**
         * Constructeur de la classe interne Views
         * @param split les 2 fenêtres de l'affichage de l'image
         * @param thumbnail la fenêtre de la miniature
         */
        public Views(JSplitPane split, Thumbnail thumbnail){

           setPreferredSize(split.getPreferredSize());

            add(split, new Integer(0), 0);
            split.setBounds(0, 0, split.getPreferredSize().width, split.getPreferredSize().height);

            thumbnail.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.black));  //Facilite la délimitation du Thumbnail
            add(thumbnail, new Integer(1), 0);

            thumbnail.setBounds(0, split.getPreferredSize().height - thumbnail.getDimension().height,
                    (int)thumbnail.getDimension().getWidth(), (int)thumbnail.getDimension().getHeight());
        }
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		this.reset();
		//views.setSize(split.getPreferredSize());
		
	}


}
