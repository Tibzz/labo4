/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: Thumbnail.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

package View;

import javax.swing.*;

import model.ImageFile;
import model.ModelThumbnail;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;



public class Thumbnail extends JComponent implements Observer{

	public static final double DIMENSION_MAX_THUMB = 150;
    
    private Image image;

    public static Dimension dimension = new Dimension(ModelThumbnail.redimensionnerThumb(ImageFile.getInstance().getDimensionImage(), DIMENSION_MAX_THUMB));

    public Thumbnail(){
        setSize(dimension);
        image = ImageFile.getInstance().getImage();
        ImageFile.getInstance().addObserver(this);
    }


    public Dimension getDimension(){
        return dimension;
    }
    
    protected void paintComponent(Graphics g){
    	g.drawImage(image,0,0,dimension.width,dimension.height,0,0,image.getWidth(null),image.getHeight(null),null);
    	}


	@Override
	public void update(Observable arg0, Object arg1) {
		dimension = ModelThumbnail.redimensionnerThumb(ImageFile.getInstance().getDimensionImage(), DIMENSION_MAX_THUMB);
		image = ImageFile.getInstance().getImage();
		repaint();
		
	}
}