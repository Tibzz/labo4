/******************************************************
Cours:  LOG121
Session: Ete 2014
Projet: Laboratoire #4

Nom du fichier: ImageViewerApplication.java

*Etudiants :
*	- Alex Leduc
*	- Alexandre Diz Ganito
*	- Thibault Nowak
*
Derniere modification: 2014-25-07
*******************************************************/

import java.io.File;

import controlleur.ImageControl;
import model.ImageFile;
import model.ModelImage;
import View.MainFrame;
import controlleur.ImageControl;





public class ImageViewerApplication {

    public static void main(String[] args){
        ImageViewerApplication app = new ImageViewerApplication();
    }

    public ImageViewerApplication(){
       
        ImageFile.getInstance().setImage(new File(ImageFile.getInstance().imageChooser()));
        
        ImageControl ctrImage = new ImageControl();
    	
    	new MainFrame(ctrImage);
        
    }
}
